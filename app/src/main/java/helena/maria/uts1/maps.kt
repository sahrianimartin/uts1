package helena.maria.uts1

import android.content.Intent
import android.graphics.Color
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.activity_maps.*
import mumayank.com.airlocationlibrary.AirLocation
import org.json.JSONArray
import org.json.JSONObject

class maps : AppCompatActivity(), View.OnClickListener, OnMapReadyCallback {

    val KEDIRI = "kediri+jawa+timur"
    val MAPBOX_TOKEN = "pk.eyJ1Ijoic2FocmlhbmltYXJ0aW4iLCJhIjoiY2tnYTh6cmN2MDR1aTJycHFtY3Exa2ZtbyJ9.EjUBlMiY0ysee1Kxhg0Eyg"
    var URL = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLokasi -> {
                URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" +
                        "$KEDIRI.json?proximity=$lng,$lat&access_token=$MAPBOX_TOKEN&limit=1"
                getDestinationLocation(URL)
            }
            R.id.fab -> {
                val ll = LatLng(lat, lng)
                gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 16.0f))
                txMyLocation.setText("My position : LAT=$lat, LNG=$lng")
            }

        }
    }
    //this function is used to get the destination's latitude and longitude
    fun getDestinationLocation(url : String) {
        val request = JsonObjectRequest(Request.Method.GET,url,null,
            Response.Listener {
                val features: JSONObject = it.getJSONArray("features").getJSONObject(0)
                val place_name : String = features.getString("place_name")
                val center : JSONArray = features.getJSONArray("center")
                val lat : String = center.get(0).toString()
                val lng : String = center.get(1).toString()
                getDestinationRoutes(lng,lat,place_name)
            }, Response.ErrorListener {
                Toast.makeText(this, "can't get destination location",Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    //this function is used to get the route to the destination
    fun getDestinationRoutes(destLat : String, destLng : String, place_name : String) {
        URL = "https://api.mapbox.com/directions/v5/mapbox/driving/" +
                "$lng,$lat;$destLng,$destLat?access_token=$MAPBOX_TOKEN&geometries=geojson"
        val request = JsonObjectRequest(Request.Method.GET,URL, null,
            Response.Listener {
                val routes : JSONObject = it.getJSONArray("routes").getJSONObject(0)
                val legs : JSONObject = routes.getJSONArray("legs").getJSONObject(0)
                val distance : Double = legs.getInt("distance")/1000.0
                val duration : Int = legs.getInt("duration")/60
                txMyLocation.setText("My Location :\nLat : $lat Lng : $lng\n" +
                        "Destination : $place_name\nLat : $destLat Lng : $destLng\n" +
                        "Distance : $distance km  Duration : $duration minute")
                val geometry : JSONObject = routes.getJSONObject("geometry")
                val coordinates: JSONArray = geometry.getJSONArray("coordinates")
                val arraySteps = ArrayList<LatLng>()
                for (i:Int in 0..coordinates.length()-1) {
                    val lngLat: JSONArray = coordinates.getJSONArray(i)
                    val fLng: Double = lngLat.getDouble(0)
                    val fLat: Double = lngLat.getDouble(1)
                    arraySteps.add(LatLng(fLat, fLng))
                }
                drawRoutes(arraySteps,place_name)
            },
            Response.ErrorListener {
                Toast.makeText(this, "something is wrong! ${it.message.toString()}",
                    Toast.LENGTH_SHORT).show()
            })
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    //this functin is used to draw the route on the map
    fun drawRoutes(array : ArrayList<LatLng>,place_name: String) {
        gMap?.clear()
        val polyline : PolylineOptions = PolylineOptions().color(Color.BLUE).width(10.0f)
            .clickable(true).addAll(array)
        gMap?.addPolyline(polyline)
        val ll = LatLng(lat,lng)
        gMap?.addMarker(MarkerOptions().position(ll).title("Hi! I'am here"))
        gMap?.addMarker(MarkerOptions().position(array.get(array.size-1)).title(place_name))
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,10.0f))
    }

    var lat : Double = 0.0; var lng : Double = 0.0;
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        mapFragment = supportFragmentManager.findFragmentById(R.id.mapsFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        btnLokasi.setOnClickListener(this)
        fab.setOnClickListener(this)
    }


    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if(gMap != null){
            airLoc = AirLocation(this, true, true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@maps, "failed to get current location",
                            Toast.LENGTH_SHORT).show()
                        txMyLocation.setText("failed to get current location")
                    }

                    override fun onSuccess(location: Location) {
                        lat = location.latitude; lng = location.longitude
                        val ll = LatLng(location.latitude,location.longitude)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Hi!I'm Here"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        txMyLocation.setText("My Position : LAT=${location.latitude},"+
                                "LNG=${location.longitude}")

                    }
                })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode,resultCode,data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode,permissions,grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}
