package helena.maria.uts1

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var fbaut = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        location.setOnClickListener(this)
        logout.setOnClickListener(this)
        resep.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.location -> {
                val intent = Intent(this, maps::class.java)
                startActivity(intent)
            }
            R.id.resep -> {
                val intent = Intent(this, file::class.java)
                startActivity(intent)
            }
            R.id.logout -> {
                out("Ingin Keluar Dari Aplikasi !!!")
            }
        }
    }

    fun out(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                fbaut.signOut()
                finish()
            }
            .setCancelClickListener { sDialog ->
                sDialog.dismissWithAnimation()
            }
            .show()
    }
}
